import { Get, Controller } from '@nestjs/common';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getOne(): string {
    return this.appService.root();
  }

  @Get('api')
  getTwo(): string {
    return this.appService.root2();
  }
}
