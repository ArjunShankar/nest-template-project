import { Module } from '@nestjs/common';
import { DevicesController } from './devices.controller';
import { MongooseModule } from '@nestjs/mongoose';
import { DevicesService } from './devices.service';
import { DeviceSchema } from './schemas/device.schema';

@Module({
  imports: [
    MongooseModule.forFeature([{ name: 'Device', schema: DeviceSchema }])       // model registered in current scope
  ],
  controllers: [DevicesController],
  providers: [DevicesService]
})
export class DevicesModule {}
