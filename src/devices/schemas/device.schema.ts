import * as mongoose from 'mongoose';

export const DeviceSchema = new mongoose.Schema({
  name: String,
  type: String,
  id: String
});