import { Injectable } from '@nestjs/common';
import { CreateDeviceDto } from './dto/create-device.dto';
import { Device } from './interfaces/device.interface';
import { Model } from 'mongoose';
import { InjectModel } from '@nestjs/mongoose';

@Injectable()
export class DevicesService {

  constructor(@InjectModel('Device') private readonly deviceModel: Model<Device>) {
  }

  async getAllDevices() {
    return await this.deviceModel.find().exec();
  }

  async getDevice(id: string) {
    return await this.deviceModel.findById(id).exec();
  }

  async addDevice(createDeviceDto: CreateDeviceDto): Promise<Device> {
    const newDevice = new this.deviceModel(createDeviceDto);
    return await newDevice.save();
  }

  async updateDevice(id: string, updateDeviceDto: CreateDeviceDto): Promise<Device> {
    const device = new this.deviceModel(updateDeviceDto);
    return await device.findOneAndUpdate(id);
  }

  async deleteDevice(id: string) {
    return await this.deviceModel.findOneAndRemove(id).exec();
  }
}
