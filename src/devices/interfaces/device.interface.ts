import { Document } from 'mongoose';

export interface Device extends Document {
  readonly name: string;
  readonly type: string;
  readonly id: string;
}