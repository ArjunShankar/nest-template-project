import { Body, Controller, Delete, Get, Param, Post, Put } from '@nestjs/common';

import { DevicesService } from './devices.service';
import { CreateDeviceDto } from './dto/create-device.dto';

@Controller('devices')      // this gets appended to the route
export class DevicesController {

  constructor(private readonly devicesService: DevicesService) {

  }

  @Get()
  async getAllDevices(): Promise<any[]> {
    return this.devicesService.getAllDevices();
  }

  @Get(':id')
  async getDevice(@Param('id') id): Promise<any[]> {
    return this.devicesService.getDevice(id);
  }

  @Post()
  async addDevice(@Body() createDeviceDto: CreateDeviceDto): Promise<any> {
     this.devicesService.addDevice(createDeviceDto);
  }

  @Put(':id')
  async updateDevice(@Param('id') id, @Body() updateDeviceDto: CreateDeviceDto): Promise<any> {
    return this.devicesService.updateDevice(id, updateDeviceDto);
  }

  @Delete(':id')
  async deleteDevice(@Param('id') id): Promise<any> {
    this.devicesService.deleteDevice(id);
  }

}