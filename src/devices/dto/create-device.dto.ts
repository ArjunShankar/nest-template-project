//Wimport { IsString } from 'class-validator';

export class CreateDeviceDto {
  readonly name: string;
  readonly type: string;
  readonly id: string;
}